Pysometric
==========

An isometric game engine written in python using pygame.

What works:

- Loading tiles and maps from files.
- Loading settings from config files.
- Drawing, zooming and scrolling maps.

What is yet to be added:

- Characters and entities
- Movement and collision
- Combat
- Interface
- Loading RPG system (dynamic from config files)
- Multiplayer
