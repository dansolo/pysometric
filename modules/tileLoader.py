#!/usr/bin/python2

import os, ConfigParser
import pygame 

config = ConfigParser.ConfigParser()

class tileObject:
    def __init__(self, name, image, collision):
        self.tileName = name
        self.origImage = image
        self.tileCollision = collision
        self.image = image

def loadTiles(tilePath):
    files = [os.path.join(tilePath, f) for f in os.listdir(tilePath) if f.endswith('.cfg')]
    tiles = []

    for f in files:
        config.read(f)
        name = config.get('tileObject', 'name')
        image = config.get('tileObject', 'image')
        image = pygame.image.load(os.path.join(tilePath, image)).convert_alpha()
        collision = config.get('tileObject', 'collision') == 'True'
        tile = tileObject(name, image, collision)
        tiles.append(tile)

    return tiles

def newTile(existingTile):
    t = existingTile
    return tileObject(t.tileName, t.image, t.tileCollision)
