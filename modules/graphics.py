#!/usr/bin/python2

import conversion
import tileLoader
import mapLoader
import pygame
from pygame.locals import *
import math

class Screen:
    def __init__(self, title, resolution, fullscreen = False):
        self.title = title
        self.resolution = resolution
        self.fullscreen = fullscreen

        ##Zooming
        self.baseZoomLevel = 1.0
        self.zoomLevel = self.baseZoomLevel
        self.maxZoomIn = 2.0
        self.maxZoomOut = 0.5

        ## Define center of screen for easier referencing
        self.center = (self.resolution[0]/2, self.resolution[1]/2)
 
        ## Create the display
        if self.fullscreen:
            self.screen = pygame.display.set_mode(resolution, pygame.FULLSCREEN)
        else:
            self.screen = pygame.display.set_mode(resolution)

        pygame.display.set_caption(title)

        ## Map offset for drawing
        self.defaultMapOffset = [self.center[0], self.screen.get_size()[1]/20]
        self.mapOffset = self.defaultMapOffset

        ## Collision accuracy 50 - 1(lower values are more accurate, however it is very slow)
        self.collisionAccuracy = 20

    def loadTiles(self, tilePath):
        ''' Loads tiles from path and adds them to Screen.tileList. Also determines the size of the tiles for drawing and conversion functions ''' 
        self.tileList = tileLoader.loadTiles(tilePath) 

       
        ## Determine the size of the tile's "base" (the ground area), this is important for drawing to the screen.
        self.origTileBaseSize = (self.tileList[0].image.get_size()[0], self.tileList[0].image.get_size()[0]/2)
        self.tileBaseSize = self.origTileBaseSize

    def resizeTiles(self, scaleFactor):
        ''' Resizes tiles in the current map by a scale factor '''
        for f in self.currentMap.mapData:
            for t in f:
                size = t.origImage.get_size()
                self.tileBaseSize = self.origTileBaseSize

                t.image = pygame.transform.scale(t.origImage, (int(round(size[0]*scaleFactor)), int(round(size[1]*scaleFactor))))
                self.tileBaseSize = (int(round(self.tileBaseSize[0]*scaleFactor)), int(round(self.tileBaseSize[1]*scaleFactor)))

    def loadMaps(self, mapPath):
        ''' Gets a list of available maps from a map path, using the map loader '''
        self.mapList = mapLoader.loadMaps(mapPath)

    def setMap(self, mapName):
        ''' Sets the current map (the one to be drawn) '''
        for i in self.mapList:
            if i.mapName == mapName:
                self.currentMap = i
                break
        else:
            print "Could not find map: ", mapName
            return 1
        for i in range(len(self.currentMap.mapData)):
            for j in range(len(self.currentMap.mapData[i])):
                for t in self.tileList:
                    if t.tileName == self.currentMap.mapData[i][j]:
                        self.currentMap.mapData[i][j] = tileLoader.newTile(t)
                        break
                else:
                    print "Cannot load tile for", self.currentMap.mapData[i][j], "."
                    raise SystemExit
            
        

    def drawMap(self):
        ''' Pretty self-explanatory. Draws the current map to the screen. '''
        mapArray = self.currentMap.mapData
        for i in range(len(mapArray)):
            for j in range(len(mapArray[i])):
                x, y = conversion.cartToIso((i, j), self)
 
                ##Offset the map
                x += self.mapOffset[0]
                y += self.mapOffset[1]
                
                ##Use the top left of the tile's "base" for blitting
                y -= mapArray[i][j].image.get_size()[1] - self.tileBaseSize[1]
                ## Save the position for reference for collision detection, etc.
                mapArray[i][j].position = (x, y)
                try:
                    self.screen.blit(mapArray[i][j].image, (x, y))
                except:
                    pass

    def zoom(self, inOrOut):
        ''' Zooms in or out. '''

        if inOrOut == 'in':
            if not self.zoomLevel >= self.maxZoomIn:
                self.zoomLevel += 0.1

        if inOrOut == 'out':
            if not self.zoomLevel <= self.maxZoomOut:
                self.zoomLevel -= 0.1

        self.resizeTiles(self.zoomLevel)

    def scrollMap(self, x, y):
        ''' Scrolls the map by x and y value. Positive x moves the map right. Positive y moves the map down. '''
        self.mapOffset[0] += x
        self.mapOffset[1] += y

    def getTileAt(self, point):
        ## This needs to be filled in
        return None
     
    def getCollisionMap(self):
        self.collisionMap = set()
        for i in range(len(self.currentMap.mapData)):
            for j in range(len(self.currentMap.mapData[i])):
                if self.currentMap.mapData[i][j].tileCollision:
                    pass
                else:
                    mask = pygame.mask.from_surface(self.currentMap.mapData[i][j].image)
                    for x in range(0, self.currentMap.mapData[i][j].image.get_size()[0], self.collisionAccuracy):
                        for y in range(0, self.currentMap.mapData[i][j].image.get_size()[1], self.collisionAccuracy):
                            if mask.get_at((x, y)):
                                position = (self.currentMap.mapData[i][j].position[0]+x, self.currentMap.mapData[i][j].position[1]+y)
                                self.collisionMap.add(position)


    def getCollisionAt(self, point):
        for i in range(point[0]-self.collisionAccuracy, point[0]+self.collisionAccuracy):
            for y in range(point[1]-self.collisionAccuracy, point[1]+self.collisionAccuracy):
                if (i, y) in self.collisionMap:
                    return False
                    break
        else:
            return True
