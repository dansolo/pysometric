#!/usr/bin/python2

def cartToIso(point, screenObject):
    x = point[1] - point[0]
    y = (point[1] + point[0])

    x = x * screenObject.tileBaseSize[0] / 2
    y = y * screenObject.tileBaseSize[1] / 2 

    return x, y

def isoToCart(point, screenObject):

    i = point[0] / (screenObject.tileBaseSize[0]/2)
    j = point[1] / (screenObject.tileBaseSize[1]/2)

    x = (j - i) / 2
    y = (j + i) / 2

    return x, y
