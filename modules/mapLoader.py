#!/usr/bin/python2
import os

class mapObject:
    def __init__(self, mapName, mapData):
        self.mapName = mapName
        self.mapData = mapData

def loadMaps(path):
    files = [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.map')]
    mapList = []
    for f in files:
        with open (f, "r") as data:
            mapData = data.read().rstrip('\n').split('\n')
            for i in range(len(mapData)):
                mapData[i] = mapData[i].split(' ')
        fileName = f.partition('.')[0]
        mapName = fileName.partition('/')[-1]
        Map = mapObject(mapName, mapData)
        mapList.append(Map)

    return mapList

