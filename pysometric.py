#!/usr/bin/python2

from modules.graphics import Screen
import pygame
import os, ConfigParser
import random
import time

mapPath = 'maps/'
tilePath = 'tiles/'
cfgFile = 'pysometric.cfg'

## Load configuration settings
config = ConfigParser.ConfigParser()
with open(os.path.join(cfgFile), "r") as c:
    config.readfp(c)
    resolution = (config.get('Graphics', 'resolution').split('x'))
    resolution = [int(i) for i in resolution]
    fullscreen = (config.get('Graphics', 'fullscreen') == 'True')

## To be in a config file later
scrollSpeed = 10
framerate = 40

screen = Screen("Pysometric", resolution, fullscreen)

screen.loadMaps(mapPath)

screen.loadTiles(tilePath)

screen.setMap('huge')

screen.drawMap()
screen.getCollisionMap()

clock = pygame.time.Clock()

##cursors
pygame.mouse.set_visible(False)
nocursor = pygame.image.load(os.path.join('gui/', 'nocursor.png')).convert_alpha()
cursor = pygame.image.load(os.path.join('gui/', 'cursor.png')).convert_alpha()

while True:

    clock.tick(framerate)

    screen.screen.fill((0, 0, 0))
    for e in pygame.event.get():
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_ESCAPE:
                raise SystemExit
            elif e.key == pygame.K_COMMA:
                screen.zoom('in')
            elif e.key == pygame.K_PERIOD:
                screen.zoom('out')
        
    keys = pygame.key.get_pressed()

    if keys[pygame.K_LEFT]:
        screen.scrollMap(scrollSpeed, 0)

    if keys[pygame.K_RIGHT]:
        screen.scrollMap(-scrollSpeed, 0)

    if keys[pygame.K_UP]:
        screen.scrollMap(0, scrollSpeed)

    if keys[pygame.K_DOWN]:
        screen.scrollMap(0, -scrollSpeed)

    screen.drawMap()

    screen.getCollisionMap()
    
    mousepos = pygame.mouse.get_pos()

    collision = screen.getCollisionAt(mousepos)

    if collision:
        screen.screen.blit(nocursor, mousepos)
    else:
        screen.screen.blit(cursor, mousepos)
    
    pygame.display.flip()

    pygame.event.pump()

